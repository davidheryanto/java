package generator;

import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import common.*;
import common.entity.Company;
import common.entity.CompanyRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.text.WordUtils;
import org.kohsuke.randname.RandomNameGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dheryanto on 18/3/2016.
 */
public class CompanyGenerator {
    public static final int SEED = 113819;
    public static Random rand = new Random(SEED);
    private static RandomNameGenerator nameGenerator = new RandomNameGenerator((int) SEED);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public static void main(String[] args) throws JAXBException {
        int dumpCount = 10;
        System.out.println(new Date() + " Getting all companies");
        List<CompanyRecord> companyRecords = getAllCompany();
        List<Map<String, String>> headers = getAllHeader("header.csv");

        for (int i = 0; i < dumpCount; i++) {
            System.out.println(new Date() + String.format(" Creating %2d-th dump", i + 1));
            List<CompanyRecord> newCompanyRecords = new ArrayList<>();
            Company company = new Company();
            for (CompanyRecord companyRecord : companyRecords) {
                if (ThresholdProb.SELECT_ROW >= rand.nextDouble()) {
                    double rowActionChance = rand.nextDouble();
                    if (rowActionChance >= ThresholdProb.DELETE_ROW) {
                        doDelete(company, companyRecord);
                    } else if (rowActionChance >= ThresholdProb.UPDATE_ROW) {
                        doUpdate(company, companyRecord);
                        newCompanyRecords.add(companyRecord);
                    } else if (rowActionChance >= ThresholdProb.ADD_ROW) {
                        doAdd(company, newCompanyRecords);
                        newCompanyRecords.add(companyRecord);
                    }
                } else {
                    newCompanyRecords.add(companyRecord);
                }
            }

            Map<String, String> header = headers.get(i);
            company.header.agencyCode = header.get("agencyCode");
            company.header.runDate = header.get("runDate");
            company.header.startDate = header.get("startDate");
            company.header.endDate = header.get("endDate");
            company.header.totalRecordCount = String.valueOf(company.companyRecords.size());

            // Write to xml
            JAXBContext jaxbContext = JAXBContext.newInstance(Company.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");

            String fileName = String.format("O_COPRO_Q_%s_%2d.xml", company.header.runDate, i + 1);
            Path filePath = Paths.get(System.getenv("DATAPATH"), "temp", fileName);
            marshaller.marshal(company, filePath.toFile());

            // Update personRecords
            companyRecords = newCompanyRecords;
        }
    }

    public static List<CompanyRecord> getAllCompany() {
        List<CompanyRecord> companyRecords = new ArrayList<>();
        try {
            Connection conn = Util.getMySqlConn("centos-1", "acra");
            assert conn != null;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from COPRO ORDER BY RAND() limit 1000000");
            ResultSetMetaData rsMetadata = rs.getMetaData();
            int colCount = rsMetadata.getColumnCount();

            while (rs.next()) {
                CompanyRecord pr = new CompanyRecord();
                for (int i = 1; i <= colCount; i++) {
                    String prop = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, rsMetadata.getColumnName(i));
                    String value = rs.getString(i);
                    Util.set(pr, prop, value);
                }
                companyRecords.add(pr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return companyRecords;
    }

    public static List<Map<String, String>> getAllHeader(String filePath) {
        List<Map<String, String>> headers = new ArrayList<>();
        try {
            String[] keys = {"agencyCode", "runDate", "startDate", "endDate"};

            Reader in = new FileReader(filePath);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
            for (CSVRecord record : records) {
                Map<String, String> header = new HashMap<>();
                for (String key : keys) {
                    header.put(key, record.get(key));
                }
                headers.add(header);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return headers;
    }

    public static void doAdd(Company company, List<CompanyRecord> newCompanyRecords) {
        CompanyRecord companyRecord = new CompanyRecord();
        for (Field field : companyRecord.getClass().getDeclaredFields()) {
            String property = field.getName();
            String value = null;
            switch (property) {
                case "coRegnNbr":
                    value = getHashFromDate(20);
                    break;
                case "coName":
                    value = getRandomName();
                    break;
                case "coTypeCode":
                case "coStatusCode":
                case "coCtryOfIncorporationCode":
                    value = getHashFromDate(2);
                    break;
                case "coRegnDate":
                case "coStatusEffDate":
                case "coNameEffDate":
                case "coAddrEffDate":
                case "coCreationDttm":
                case "coRecordLastModifiedDttm":
                    value = getCurrentDateString();
                    break;
                case "coPostalCode":
                case "coBlkHseNbr":
                case "coStrName":
                case "coPrimaryActivityCode":
                case "coSecondaryActivityCode":
                case "coBldName":
                case "coLevelNbr":
                case "coUnitNbr":
                case "coForeignName":
                case "coType":
                case "coStatus":
                case "coCtryOfIncorporation":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(5);
                    }
                    break;
                case "coAuthorisedCapitalNbr":
                case "coPaidupCapitalNbr":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        long randVal = rand.nextInt(1000) * 1000000000000L;
                        value = String.format("%030d", randVal);
                    }
                    break;
                case "coNbrOfMembers":
                    value = String.valueOf(rand.nextInt(10000));
                    break;
                case "coRecordTypeCode":
                    value = "A";
                    break;
                case "coSeqNbr":
                    value = getHashFromDate(15);
                    break;
                case "coPrimaryActivityAddlDescr":
                case "coSecondaryActivityAddlDescr":
                case "coSecondaryActivity":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(100);
                    }
                    break;
                case "coAllotteeInd":
                case "coCnvInd":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        if (rand.nextDouble() >= 0.50) {
                            value = "Y";
                        } else {
                            value = "N";
                        }
                    }
                    break;
                default:
                    value = null;
                    break;
            }
            Util.set(companyRecord, property, value);
        }
        company.add(companyRecord);
        newCompanyRecords.add(companyRecord);
    }


    public static void doUpdate(Company company, CompanyRecord companyRecord) {
        // Delete row
        CompanyRecord newEntry = new CompanyRecord();
        newEntry.coRegnNbr = companyRecord.coRegnNbr;
        assert newEntry.coRegnNbr != null;
        newEntry.coRecordTypeCode = "D";
        newEntry.coRecordLastModifiedDttm = getCurrentDateString();
        company.add(newEntry);

        String companyFormerBizRegnNbr = newEntry.coFormerBizRegnNbr;

        // Add row
        newEntry = new CompanyRecord();
        newEntry.coRecordTypeCode = "A";

        // Currenly these properties can be updated:
        // personIdNbr, personName, personStrName based on chance
        double chance = rand.nextDouble();
        if (chance < 0.33) {
            newEntry.coFormerBizRegnNbr = companyRecord.coFormerBizRegnNbr = companyFormerBizRegnNbr;
            newEntry.coRegnNbr = companyRecord.coRegnNbr = getHashFromDate(20);
            companyFormerBizRegnNbr = newEntry.coRegnNbr;
        } else if (chance < 0.66) {
            newEntry.coFormerBizRegnNbr = companyRecord.coFormerBizRegnNbr = companyFormerBizRegnNbr;
            newEntry.coRegnNbr = companyRecord.coRegnNbr = getHashFromDate(20);
            newEntry.coName = companyRecord.coName = getRandomName();
            companyFormerBizRegnNbr = newEntry.coRegnNbr;
        } else {
            newEntry.coName = companyRecord.coName = getRandomName();
        }
        newEntry.coRecordLastModifiedDttm = getCurrentDateString();
        company.add(newEntry);

        if (rand.nextDouble() >= ThresholdProb.ADD_ROW / 2.0) {
            // Double edit in same day, the prob for consecutive edit is made much lower

            // Delete row
            newEntry = new CompanyRecord();
            newEntry.coRegnNbr = companyFormerBizRegnNbr;
            assert newEntry.coRegnNbr != null;
            newEntry.coRecordTypeCode = "D";
            newEntry.coRecordLastModifiedDttm = getCurrentDateString();
            company.add(newEntry);

            // Add row
            newEntry = new CompanyRecord();
            newEntry.coRecordTypeCode = "A";

            // Currenly these properties can be updated:
            // personIdNbr, personName, personStrName based on chance
            chance = rand.nextDouble();
            if (chance < 0.33) {
                newEntry.coFormerBizRegnNbr = companyRecord.coFormerBizRegnNbr = companyFormerBizRegnNbr;
                newEntry.coRegnNbr = companyRecord.coRegnNbr = getHashFromDate(20);
            } else if (chance < 0.66) {
                newEntry.coFormerBizRegnNbr = companyRecord.coFormerBizRegnNbr = companyFormerBizRegnNbr;
                newEntry.coRegnNbr = companyRecord.coRegnNbr = getHashFromDate(20);
                newEntry.coName = companyRecord.coName = getRandomName();
            } else {
                newEntry.coName = companyRecord.coName = getRandomName();
            }
            newEntry.coRecordLastModifiedDttm = getCurrentDateString();
            company.add(newEntry);
        }
    }

    public static void doDelete(Company company, CompanyRecord companyRecord) {
        // Delete row
        CompanyRecord newEntry = new CompanyRecord();
        newEntry.coRegnNbr = companyRecord.coRegnNbr;
        assert newEntry.coRegnNbr != null;
        newEntry.coRecordTypeCode = "D";
        newEntry.coRecordLastModifiedDttm = getCurrentDateString();
        company.add(newEntry);
    }

    public static String getSha256(String text, int length) {
        MessageDigest md;
        String hashString = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            HexBinaryAdapter hexBinaryAdapter = new HexBinaryAdapter();
            md.update(text.getBytes());
            byte[] hash = md.digest();
            hashString = hexBinaryAdapter.marshal(hash);
            if (hashString.length() > length) {
                hashString = hashString.substring(0, length);
            } else if (hashString.length() < length) {
                Strings.padStart(hashString, length, 'A');
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashString;
    }

    public static String getHashFromDate(int length) {
        return getSha256(String.valueOf(System.currentTimeMillis() + rand.nextDouble()), length);
    }

    public static String getCurrentDateString() {
        return dateFormat.format(new java.util.Date());
    }

    public static String getRandomName() {
        return WordUtils.capitalizeFully(nameGenerator.next().replace('_', ' '));
    }
}
