package generator;

import com.google.common.base.CaseFormat;
import common.entity.Person;
import common.entity.PersonRecord;
import common.ThresholdProb;
import common.Util;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.text.WordUtils;
import org.kohsuke.randname.RandomNameGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dheryanto on 18/3/2016.
 */
public class PersonGenerator {
    public static final int SEED = 113819;
    public static Random rand = new Random(SEED);
    private static RandomNameGenerator nameGenerator = new RandomNameGenerator((int) SEED);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public static void main(String[] args) throws JAXBException {
        int dumpCount = 10;
        System.out.println(new Date() + " Getting all persons");
        List<PersonRecord> personRecords = getAllPerson();
        List<Map<String, String>> headers = getAllHeader("header.csv");

        for (int i = 0; i < dumpCount; i++) {
            System.out.println(new Date() + String.format(" Creating %2d-th dump", i + 1));
            List<PersonRecord> newPersonRecords = new ArrayList<>();
            Person person = new Person();
            for (PersonRecord personRecord : personRecords) {
                if (ThresholdProb.SELECT_ROW >= rand.nextDouble()) {
                    double rowActionChance = rand.nextDouble();
                    if (rowActionChance >= ThresholdProb.DELETE_ROW) {
                        doDelete(person, personRecord);
                    } else if (rowActionChance >= ThresholdProb.UPDATE_ROW) {
                        doUpdate(person, personRecord);
                        newPersonRecords.add(personRecord);
                    } else if (rowActionChance >= ThresholdProb.ADD_ROW) {
                        doAdd(person, newPersonRecords);
                        newPersonRecords.add(personRecord);
                    }
                } else {
                    newPersonRecords.add(personRecord);
                }
            }

            Map<String, String> header = headers.get(i);
            person.header.agencyCode = header.get("agencyCode");
            person.header.runDate = header.get("runDate");
            person.header.startDate = header.get("startDate");
            person.header.endDate = header.get("endDate");
            person.header.totalRecordCount = String.valueOf(person.personRecords.size());

            // Write to xml
            JAXBContext jaxbContext = JAXBContext.newInstance(Person.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");

            String fileName = String.format("O_PP_Q_%s_%2d.xml", person.header.runDate, i + 1);
            Path filePath = Paths.get(System.getenv("DATAPATH"), "temp", fileName);
            marshaller.marshal(person, filePath.toFile());

            // Update personRecords
            personRecords = newPersonRecords;
        }
    }

    public static List<PersonRecord> getAllPerson() {
        List<PersonRecord> personRecords = new ArrayList<>();
        try {
            Connection conn = Util.getMySqlConn("centos-1", "acra");
            assert conn != null;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from PP");
            ResultSetMetaData rsMetadata = rs.getMetaData();
            int colCount = rsMetadata.getColumnCount();

            while (rs.next()) {
                PersonRecord pr = new PersonRecord();
                for (int i = 1; i <= colCount; i++) {
                    String prop = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, rsMetadata.getColumnName(i));
                    String value = rs.getString(i);
                    Util.set(pr, prop, value);
                }
                personRecords.add(pr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return personRecords;
    }

    public static List<Map<String, String>> getAllHeader(String filePath) {
        List<Map<String, String>> headers = new ArrayList<>();
        try {
            String[] keys = {"agencyCode", "runDate", "startDate", "endDate"};

            Reader in = new FileReader(filePath);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
            for (CSVRecord record : records) {
                Map<String, String> header = new HashMap<>();
                for (String key : keys) {
                    header.put(key, record.get(key));
                }
                headers.add(header);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return headers;
    }

    public static void doAdd(Person person, List<PersonRecord> newPersonRecords) {
        PersonRecord personRecord = new PersonRecord();
        for (Field field : personRecord.getClass().getDeclaredFields()) {
            String property = field.getName();
            String value = null;
            switch (property) {
                case "personIdNbr":
                    value = getHashFromDate(20);
                    break;
                case "personName":
                    value = getRandomName();
                    break;
                case "personIdTypeCode":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = String.valueOf(1 + rand.nextInt(4));
                    }
                    break;
                case "personNationalityCode":
                    value = getHashFromDate(2);
                    break;
                case "personAddrSourceCode":
                    value = "R";
                    break;
                case "personAddrTypeCode":
                    if (rand.nextDouble() >= 0.50) {
                        value = "S";
                    } else {
                        value = "F";
                    }
                    break;
                case "personForeignAddrLine1":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personForeignAddrLine2":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personDeathInd":
                    if (rand.nextDouble() >= 0.999) {
                        value = "Y";
                    } else {
                        value = "N";
                    }
                    break;
                case "personRecordTypeCode":
                    value = "A";
                    break;
                case "personCreationDttm":
                    value = getCurrentDateString();
                    break;
                case "personSeqNbr":
                    value = getHashFromDate(15);
                    break;
                case "personChgOfAddrDate":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getCurrentDateString();
                    }
                    break;
                case "personPostalCode":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(6);
                    }
                    break;
                case "personBlkHseNbr":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personStrName":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(32);
                    }
                    break;
                case "personLevelNbr":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = String.valueOf(rand.nextInt(1000));
                    }
                    break;
                case "personUnitNbr":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = String.valueOf(rand.nextInt(1000));
                    }
                    break;
                case "personRecordLastModifiedDttm":
                    value = getCurrentDateString();
                    break;
                case "personBldName":
                    if (rand.nextDouble() >= ThresholdProb.NULL_COL) {
                        value = getHashFromDate(32);
                    }
                    break;
                default:
                    value = null;
                    break;
            }
            Util.set(personRecord, property, value);
        }
        person.add(personRecord);
        newPersonRecords.add(personRecord);
    }


    public static void doUpdate(Person person, PersonRecord personRecord) {
        // Delete row
        PersonRecord newEntry = new PersonRecord();
        newEntry.personIdNbr = personRecord.personIdNbr;
        assert newEntry.personIdNbr != null;
        newEntry.personRecordTypeCode = "D";
        newEntry.personRecordLastModifiedDttm = getCurrentDateString();
        person.add(newEntry);

        String personPreviousIdNbr = newEntry.personIdNbr;

        // Add row
        newEntry = new PersonRecord();
        newEntry.personRecordTypeCode = "A";

        // Currenly these properties can be updated:
        // personIdNbr, personName, personStrName based on chance
        double chance = rand.nextDouble();
        if (chance < 0.33) {
            newEntry.personPreviousIdNbr = personRecord.personPreviousIdNbr = personPreviousIdNbr;
            newEntry.personIdNbr = personRecord.personIdNbr = getHashFromDate(20);
            personPreviousIdNbr = newEntry.personIdNbr;
        } else if (chance < 0.66) {
            newEntry.personPreviousIdNbr = personRecord.personPreviousIdNbr = personPreviousIdNbr;
            newEntry.personIdNbr = personRecord.personIdNbr = getHashFromDate(20);
            newEntry.personName = personRecord.personName = getRandomName();
            personPreviousIdNbr = newEntry.personIdNbr;
        } else {
            newEntry.personName = personRecord.personName = getRandomName();
        }
        newEntry.personRecordLastModifiedDttm = getCurrentDateString();
        person.add(newEntry);

        if (rand.nextDouble() >= ThresholdProb.ADD_ROW / 2.0) {
            // Double edit in same day, the prob for consecutive edit is made much lower

            // Delete row
            newEntry = new PersonRecord();
            newEntry.personIdNbr = personPreviousIdNbr;
            assert newEntry.personIdNbr != null;
            newEntry.personRecordTypeCode = "D";
            newEntry.personRecordLastModifiedDttm = getCurrentDateString();
            person.add(newEntry);

            // Add row
            newEntry = new PersonRecord();
            newEntry.personRecordTypeCode = "A";

            // Currenly these properties can be updated:
            // personIdNbr, personName, personStrName based on chance
            chance = rand.nextDouble();
            if (chance < 0.33) {
                newEntry.personPreviousIdNbr = personRecord.personPreviousIdNbr = personPreviousIdNbr;
                newEntry.personIdNbr = personRecord.personIdNbr = getHashFromDate(20);
            } else if (chance < 0.66) {
                newEntry.personPreviousIdNbr = personRecord.personPreviousIdNbr = personPreviousIdNbr;
                newEntry.personIdNbr = personRecord.personIdNbr = getHashFromDate(20);
                newEntry.personName = personRecord.personName = getRandomName();
            } else {
                newEntry.personName = personRecord.personName = getRandomName();
            }
            newEntry.personRecordLastModifiedDttm = getCurrentDateString();
            person.add(newEntry);
        }


    }

    public static void doDelete(Person person, PersonRecord personRecord) {
        // Delete row
        PersonRecord newEntry = new PersonRecord();
        newEntry.personIdNbr = personRecord.personIdNbr;
        assert newEntry.personIdNbr != null;
        newEntry.personRecordTypeCode = "D";
        newEntry.personRecordLastModifiedDttm = getCurrentDateString();
        person.add(newEntry);
    }

    public static String getSha256(String text, int length) {
        MessageDigest md;
        String hashString = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            HexBinaryAdapter hexBinaryAdapter = new HexBinaryAdapter();
            md.update(text.getBytes());
            byte[] hash = md.digest();
            hashString = hexBinaryAdapter.marshal(hash).substring(0, length);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashString;
    }

    public static String getHashFromDate(int length) {
        return getSha256(String.valueOf(System.currentTimeMillis() + rand.nextDouble()), length);
    }

    public static String getCurrentDateString() {
        return dateFormat.format(new java.util.Date());
    }

    public static String getRandomName() {
        return WordUtils.capitalizeFully(nameGenerator.next().replace('_', ' '));
    }
}
