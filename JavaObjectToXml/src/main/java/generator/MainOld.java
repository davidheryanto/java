package generator;

import com.google.common.base.CaseFormat;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import common.entity.Person;
import common.entity.PersonRecord;
import common.ThresholdProb;
import org.apache.commons.lang3.text.WordUtils;
import org.kohsuke.randname.RandomNameGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by dheryanto on 17/3/2016.
 */
public class MainOld {
    private static long SEED = 120123;
    private static Random rand = new Random(SEED);
    private static RandomNameGenerator nameGenerator = new RandomNameGenerator((int) SEED);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public static void main(String[] args) throws JAXBException, ClassNotFoundException, SQLException, NoSuchAlgorithmException, IOException {
        Person person = new Person();

        // Person header
        person.header.agencyCode = "BIZHUB";
        person.header.runDate = "20140902";
        person.header.startDate = "18810101";
        person.header.endDate = "20140831";

        // SqlServer connection
        //        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        //        Connection conn = DriverManager.getConnection("jdbc:sqlserver://sgsgaapp76:1433;" +
        //                "databaseName=ACRA_2014_10_cleaned;integratedSecurity=true;");

        // MySQL connection
        Path configPath = Paths.get(System.getProperty("user.home"), "my.ini");
        FileInputStream input = new FileInputStream(configPath.toFile().getAbsolutePath());
        Properties prop = new Properties();
        prop.load(input);

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName("centos-1");
        dataSource.setDatabaseName("acra");
        dataSource.setUser(prop.getProperty("acra.user"));
        dataSource.setPassword(prop.getProperty("acra.password"));
        Connection conn = dataSource.getConnection();

        Statement stmt = conn.createStatement();

        // Person records
        ResultSet resultSet = stmt.executeQuery("SELECT * FROM PP limit 50");
        // ResultSet resultSet = stmt.executeQuery("SELECT top 50 * FROM O_PP_Q ORDER BY NEWID()");  // random rows
        int personCount = 0;

        List<String> stmtListToExecute = new ArrayList<>();
        while (resultSet.next()) {
            personCount++;

            if (ThresholdProb.SELECT_ROW >= rand.nextDouble()) {
                double rowActionChance = rand.nextDouble();
                if (rowActionChance >= ThresholdProb.DELETE_ROW) {
                    doDelete(person, resultSet, stmtListToExecute);
                } else if (rowActionChance >= ThresholdProb.UPDATE_ROW) {
                    doUpdate(person, resultSet, stmtListToExecute);
                } else if (rowActionChance >= ThresholdProb.ADD_ROW) {
                    doAdd(person, resultSet, stmtListToExecute);
                }
            }
        }

        // Set missing header info
        person.header.totalRecordCount = String.valueOf(personCount);

        // Write to xml
        JAXBContext jaxbContext = JAXBContext.newInstance(Person.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");

        marshaller.marshal(person, System.out);
        //        marshaller.marshal(person, new File("C:/Users/Public/Shared/O_PP_Q_20160318_01.XML"));
    }

    public static void doAdd(Person person, ResultSet resultSet, List<String> stmtListToExecute) throws SQLException {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();
        double nullColumnProbThreshold = 0.20;
        PersonRecord personRecord = new PersonRecord();

        // For SQL insert statement later
        List<String> sqlColumns = new ArrayList<>();
        List<String> sqlValues = new ArrayList<>();

        for (int i = 1; i <= columnCount; i++) {
            String property = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, resultSetMetaData.getColumnName(i));
            String value = null;
            switch (property) {
                case "personIdNbr":
                    value = getHashFromDate(20);
                    break;
                case "personName":
                    value = getRandomName();
                    break;
                case "personIdTypeCode":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = String.valueOf(1 + rand.nextInt(4));
                    }
                    break;
                case "personNationalityCode":
                    value = getHashFromDate(2);
                    break;
                case "personAddrSourceCode":
                    value = "R";
                    break;
                case "personAddrTypeCode":
                    if (rand.nextDouble() >= 0.50) {
                        value = "S";
                    } else {
                        value = "F";
                    }
                    break;
                case "personForeignAddrLine1":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personForeignAddrLine2":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personDeathInd":
                    if (rand.nextDouble() >= 0.999) {
                        value = "Y";
                    } else {
                        value = "N";
                    }
                    break;
                case "personRecordTypeCode":
                    value = "A";
                    break;
                case "personCreationDttm":
                    value = getCurrentDateString();
                    break;
                case "personSeqNbr":
                    value = getHashFromDate(15);
                    break;
                case "personChgOfAddrDate":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getCurrentDateString();
                    }
                    break;
                case "personPostalCode":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(6);
                    }
                    break;
                case "personBlkHseNbr":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(10);
                    }
                    break;
                case "personStrName":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(32);
                    }
                    break;
                case "personLevelNbr":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = String.valueOf(rand.nextInt(1000));
                    }
                    break;
                case "personUnitNbr":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = String.valueOf(rand.nextInt(1000));
                    }
                    break;
                case "personRecordLastModifiedDttm":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getCurrentDateString();
                    }
                    break;
                case "personBldName":
                    if (rand.nextDouble() >= nullColumnProbThreshold) {
                        value = getHashFromDate(32);
                    }
                    break;
                default:
                    value = null;
                    break;
            }
            set(personRecord, property, value);
            if (value != null) {
                sqlColumns.add(property);
                sqlValues.add(value);
            }
        }
        person.add(personRecord);

        // Add sql statement
        String query = String.format("insert into PP (%s) values (%s)",
                String.join(",", sqlColumns), String.join(",", sqlValues));
        stmtListToExecute.add(query);
    }

    public static void doUpdate(Person person, ResultSet resultSet, List<String> stmtListToExecute) throws SQLException {
        // Delete row
        PersonRecord personRecord = new PersonRecord();
        personRecord.personIdNbr = resultSet.getString("PERSON_ID_NBR");
        assert personRecord.personIdNbr != null;
        personRecord.personRecordTypeCode = "D";
        person.add(personRecord);

        String personPreviousIdNbr = personRecord.personIdNbr;

        List<String> sqlSetColValues = new ArrayList<>();

        // Add row
        personRecord = new PersonRecord();
        personRecord.personRecordTypeCode = "A";

        // Currenly these properties can be updated:
        // personIdNbr, personName, personStrName based on chance
        double chance = rand.nextDouble();
        if (chance < 0.33) {
            personRecord.personPreviousIdNbr = personPreviousIdNbr;
            personRecord.personIdNbr = getHashFromDate(20);
            personPreviousIdNbr = personRecord.personIdNbr;
        } else if (chance < 0.66) {
            personRecord.personPreviousIdNbr = personPreviousIdNbr;
            personRecord.personIdNbr = getHashFromDate(20);
            personRecord.personName = getRandomName();
            personPreviousIdNbr = personRecord.personIdNbr;
        } else {
            personRecord.personName = getRandomName();
        }
        person.add(personRecord);

        if (rand.nextDouble() >= ThresholdProb.ADD_ROW / 2.0) {
            // Double edit in same day, the prob for consecutive edit is made much lower

            // Delete row
            personRecord = new PersonRecord();
            personRecord.personIdNbr = personPreviousIdNbr;
            assert personRecord.personIdNbr != null;
            personRecord.personRecordTypeCode = "D";
            person.add(personRecord);

            // Add row
            personRecord = new PersonRecord();
            personRecord.personRecordTypeCode = "A";

            // Currenly these properties can be updated:
            // personIdNbr, personName, personStrName based on chance
            chance = rand.nextDouble();
            if (chance < 0.33) {
                personRecord.personPreviousIdNbr = personPreviousIdNbr;
                personRecord.personIdNbr = getHashFromDate(20);
            } else if (chance < 0.66) {
                personRecord.personPreviousIdNbr = personPreviousIdNbr;
                personRecord.personIdNbr = getHashFromDate(20);
                personRecord.personName = getRandomName();
            } else {
                personRecord.personName = getRandomName();
            }
            person.add(personRecord);
        }


    }

    public static void doDelete(Person person, ResultSet resultSet, List<String> stmtListToExecute) throws SQLException {
        // Delete row
        PersonRecord personRecord = new PersonRecord();
        personRecord.personIdNbr = resultSet.getString("PERSON_ID_NBR");
        assert personRecord.personIdNbr != null;
        personRecord.personRecordTypeCode = "D";
        person.add(personRecord);
    }

    /**
     * Use reflection to set object property
     * http://stackoverflow.com/questions/14374878/using-reflection-to-set-an-object-property
     *
     * @param object
     * @param property
     * @param value
     */
    public static void set(Object object, String property, Object value) {
        Class<?> clazz = object.getClass();
        if (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(property);
                field.setAccessible(true);
                field.set(object, value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                // Ignore exception
            }
        }
    }

    public static String getSha256(String text, int length) {
        MessageDigest md;
        String hashString = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            HexBinaryAdapter hexBinaryAdapter = new HexBinaryAdapter();
            md.update(text.getBytes());
            byte[] hash = md.digest();
            hashString = hexBinaryAdapter.marshal(hash).substring(0, length);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashString;
    }

    public static String getHashFromDate(int length) {
        return getSha256(String.valueOf(System.currentTimeMillis() + rand.nextDouble()), length);
    }

    public static String getCurrentDateString() {
        return dateFormat.format(new Date());
    }

    public static String getRandomName() {
        return WordUtils.capitalizeFully(nameGenerator.next().replace('_', ' '));
    }
}
