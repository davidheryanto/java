package generator;

import common.Util;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dheryanto on 18/3/2016.
 */
public class Temp {
    public static void main(String[] args) throws IOException, SQLException {
        Connection conn = Util.getMySqlConn("centos-1", "acra");
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery("select * from COPOS limit 1");
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int colCount = resultSetMetaData.getColumnCount();
        List<String> properties = new ArrayList<>();

        while (resultSet.next()) {
            for (int i = 1; i < colCount; i++) {
                properties.add(String.format("'%s'", resultSetMetaData.getColumnName(i)));
            }
        }
        System.out.println(String.join(", ", properties));
    }


}
