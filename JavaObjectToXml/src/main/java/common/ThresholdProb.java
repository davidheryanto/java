package common;

/**
 * Created by dheryanto on 17/3/2016.
 */
public class ThresholdProb {
    public final static double SELECT_ROW = 0.025;
    public final static double ADD_ROW = 0.00;
    public final static double UPDATE_ROW = 0.35;
    public final static double DELETE_ROW = 0.80;
    public final static double NULL_COL = 0.15;

}
