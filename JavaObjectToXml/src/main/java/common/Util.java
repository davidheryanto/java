package common;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.commons.lang3.text.WordUtils;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Created by dheryanto on 18/3/2016.
 */
public class Util {
    public static Connection getMySqlConn(String hostname, String database) {
        Path configPath = Paths.get(System.getProperty("user.home"), "my.ini");
        FileInputStream input = null;
        try {
            input = new FileInputStream(configPath.toFile().getAbsolutePath());
            Properties prop = new Properties();
            prop.load(input);

            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setServerName("centos-1");
            dataSource.setDatabaseName("acra");
            dataSource.setUser(prop.getProperty("acra.user"));
            dataSource.setPassword(prop.getProperty("acra.password"));

            return dataSource.getConnection();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Use reflection to set object property
     * http://stackoverflow.com/questions/14374878/using-reflection-to-set-an-object-property
     *
     * @param object
     * @param property
     * @param value
     */
    public static void set(Object object, String property, Object value) {
        Class<?> clazz = object.getClass();
        if (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(property);
                field.setAccessible(true);
                field.set(object, value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                // Ignore exception
            }
        }
    }

    public static String getSha256(String text, int length) {
        MessageDigest md;
        String hashString = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            HexBinaryAdapter hexBinaryAdapter = new HexBinaryAdapter();
            md.update(text.getBytes());
            byte[] hash = md.digest();
            hashString = hexBinaryAdapter.marshal(hash).substring(0, length);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashString;
    }

}
