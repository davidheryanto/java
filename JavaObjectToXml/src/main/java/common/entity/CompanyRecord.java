package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by dheryanto on 18/3/2016.
 */
@XmlRootElement(name = "CompanyRecord")
@XmlType(propOrder = {"coRegnNbr","coName","coTypeCode","coRegnDate","coStatusCode","coStatusEffDate","coNameEffDate","coPostalCode","coBlkHseNbr","coStrName","coAddrEffDate","coPrimaryActivityCode","coSecondaryActivityCode","coAuthorisedCapitalNbr","coPaidupCapitalNbr","coCtryOfIncorporationCode","coNbrOfMembers","coRecordTypeCode","coCreationDttm","coSeqNbr","coRecordLastModifiedDttm","coPrimaryActivityAddlDescr","coBldName","coLevelNbr","coUnitNbr","coAllotteeInd","coSecondaryActivityAddlDescr","coCnvInd","coFormerBizRegnNbr","coForeignName","coType","coStatus","coCtryOfIncorporation","coSecondaryActivity"})
public class CompanyRecord {
    @XmlElement(name="Co_Regn_Nbr")
    public String coRegnNbr;
    @XmlElement(name="Co_Name")
    public String coName;
    @XmlElement(name="Co_Type_Code")
    public String coTypeCode;
    @XmlElement(name="Co_Regn_Date")
    public String coRegnDate;
    @XmlElement(name="Co_Status_Code")
    public String coStatusCode;
    @XmlElement(name="Co_Status_Eff_Date")
    public String coStatusEffDate;
    @XmlElement(name="Co_Name_Eff_Date")
    public String coNameEffDate;
    @XmlElement(name="Co_Postal_Code")
    public String coPostalCode;
    @XmlElement(name="Co_Blk_Hse_Nbr")
    public String coBlkHseNbr;
    @XmlElement(name="Co_Str_Name")
    public String coStrName;
    @XmlElement(name="Co_Addr_Eff_Date")
    public String coAddrEffDate;
    @XmlElement(name="Co_Primary_Activity_Code")
    public String coPrimaryActivityCode;
    @XmlElement(name="Co_Secondary_Activity_Code")
    public String coSecondaryActivityCode;
    @XmlElement(name="Co_Authorised_Capital_Nbr")
    public String coAuthorisedCapitalNbr;
    @XmlElement(name="Co_Paidup_Capital_Nbr")
    public String coPaidupCapitalNbr;
    @XmlElement(name="Co_Ctry_Of_Incorporation_Code")
    public String coCtryOfIncorporationCode;
    @XmlElement(name="Co_Nbr_Of_Members")
    public String coNbrOfMembers;
    @XmlElement(name="Co_Record_Type_Code")
    public String coRecordTypeCode;
    @XmlElement(name="Co_Creation_Dttm")
    public String coCreationDttm;
    @XmlElement(name="Co_Seq_Nbr")
    public String coSeqNbr;
    @XmlElement(name="Co_Record_Last_Modified_Dttm")
    public String coRecordLastModifiedDttm;
    @XmlElement(name="Co_Primary_Activity_Addl_Descr")
    public String coPrimaryActivityAddlDescr;
    @XmlElement(name="Co_Bld_Name")
    public String coBldName;
    @XmlElement(name="Co_Level_Nbr")
    public String coLevelNbr;
    @XmlElement(name="Co_Unit_Nbr")
    public String coUnitNbr;
    @XmlElement(name="Co_Allottee_Ind")
    public String coAllotteeInd;
    @XmlElement(name="Co_Secondary_Activity_Addl_Descr")
    public String coSecondaryActivityAddlDescr;
    @XmlElement(name="Co_Cnv_Ind")
    public String coCnvInd;
    @XmlElement(name="Co_Former_Biz_Regn_Nbr")
    public String coFormerBizRegnNbr;
    @XmlElement(name="Co_Foreign_Name")
    public String coForeignName;
    @XmlElement(name="Co_Type")
    public String coType;
    @XmlElement(name="Co_Status")
    public String coStatus;
    @XmlElement(name="Co_Ctry_Of_Incorporation")
    public String coCtryOfIncorporation;
    @XmlElement(name="Co_Secondary_Activity")
    public String coSecondaryActivity;
}
