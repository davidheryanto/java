package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by dheryanto on 21/3/2016.
 */
@XmlRootElement(name = "CompanyPositionRecord")
@XmlType(propOrder = {"coOfcrPosnRegnNbr","coOfcrPosnIdCode","coOfcrPosnPosnHeldCode","coOfcrPosnApptDate","coOfcrPosnWithdrawalDate","coOfcrPosnWithdrawalRsnCode","coOfcrPosnRecordTypeCode","coOfcrPosnCreationDttm","coOfcrPosnSeqNbr","coOfcrPosnRecordLastModifiedDttm","coOfcrPosnReinstatedInd","coOfcrPosnPosnHeld","coOfcrPosnWithdrawalReason","kpmgRefIntegrityCheckPassed"})
public class CompanyPositionRecord {
    @XmlElement(name="Co_Ofcr_Posn_Regn_Nbr")
    public String coOfcrPosnRegnNbr;
    @XmlElement(name="Co_Ofcr_Posn_Id_Code")
    public String coOfcrPosnIdCode;
    @XmlElement(name="Co_Ofcr_Posn_Posn_Held_Code")
    public String coOfcrPosnPosnHeldCode;
    @XmlElement(name="Co_Ofcr_Posn_Appt_Date")
    public String coOfcrPosnApptDate;
    @XmlElement(name="Co_Ofcr_Posn_Withdrawal_Date")
    public String coOfcrPosnWithdrawalDate;
    @XmlElement(name="Co_Ofcr_Posn_Withdrawal_Rsn_Code")
    public String coOfcrPosnWithdrawalRsnCode;
    @XmlElement(name="Co_Ofcr_Posn_Record_Type_Code")
    public String coOfcrPosnRecordTypeCode;
    @XmlElement(name="Co_Ofcr_Posn_Creation_Dttm")
    public String coOfcrPosnCreationDttm;
    @XmlElement(name="Co_Ofcr_Posn_Seq_Nbr")
    public String coOfcrPosnSeqNbr;
    @XmlElement(name="Co_Ofcr_Posn_Record_Last_Modified_Dttm")
    public String coOfcrPosnRecordLastModifiedDttm;
    @XmlElement(name="Co_Ofcr_Posn_Reinstated_Ind")
    public String coOfcrPosnReinstatedInd;
    @XmlElement(name="Co_Ofcr_Posn_Posn_Held")
    public String coOfcrPosnPosnHeld;
    @XmlElement(name="Co_Ofcr_Posn_Withdrawal_Reason")
    public String coOfcrPosnWithdrawalReason;
}
