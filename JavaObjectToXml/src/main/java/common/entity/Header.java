package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by dheryanto on 17/3/2016.
 */
@XmlRootElement(name="Header")
@XmlType(propOrder = {"agencyCode","runDate","startDate","endDate","totalRecordCount"})
public class Header {
    @XmlElement(name="Agency_Code")
    public String agencyCode;
    @XmlElement(name="Run_Date")
    public String runDate;
    @XmlElement(name="Start_Date")
    public String startDate;
    @XmlElement(name="End_Date")
    public String endDate;
    @XmlElement(name="Total_Record_Count")
    public String totalRecordCount;
}
