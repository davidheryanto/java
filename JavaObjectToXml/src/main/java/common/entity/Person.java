package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dheryanto on 17/3/2016.
 */
@XmlRootElement(name = "Person")
@XmlType(propOrder = {"header", "personRecords"})
public class Person {
    @XmlElementWrapper(name = "PersonTable")
    @XmlElement(name = "PersonRecord")
    public List<PersonRecord> personRecords = new ArrayList<>();

    @XmlElement(name = "Header")
    public Header header = new Header();

    public void add(PersonRecord personRecord) {
        personRecords.add(personRecord);
    }
}
