package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by dheryanto on 17/3/2016.
 */
@XmlRootElement(name = "PersonRecord")
@XmlType(propOrder = {"personIdNbr","personName","personIdTypeCode","personNationalityCode","personAddrSourceCode","personAddrTypeCode","personForeignAddrLine1","personForeignAddrLine2","personDeathInd","personRecordTypeCode","personCreationDttm","personSeqNbr","personChgOfAddrDate","personPostalCode","personBlkHseNbr","personStrName","personLevelNbr","personUnitNbr","personRecordLastModifiedDttm","personBldName","personPreviousIdNbr","personChgOfAddrTransNbr","personChgOfIdNbrDate","personIdType","personNationality"})
public class PersonRecord {
    @XmlElement(name="Person_Id_Nbr")
    public String personIdNbr;
    @XmlElement(name="Person_Name")
    public String personName;
    @XmlElement(name="Person_Id_Type_Code")
    public String personIdTypeCode;
    @XmlElement(name="Person_Nationality_Code")
    public String personNationalityCode;
    @XmlElement(name="Person_Addr_Source_Code")
    public String personAddrSourceCode;
    @XmlElement(name="Person_Addr_Type_Code")
    public String personAddrTypeCode;
    @XmlElement(name="Person_Foreign_Addr_Line1")
    public String personForeignAddrLine1;
    @XmlElement(name="Person_Foreign_Addr_Line2")
    public String personForeignAddrLine2;
    @XmlElement(name="Person_Death_Ind")
    public String personDeathInd;
    @XmlElement(name="Person_Record_Type_Code")
    public String personRecordTypeCode;
    @XmlElement(name="Person_Creation_Dttm")
    public String personCreationDttm;
    @XmlElement(name="Person_Seq_Nbr")
    public String personSeqNbr;
    @XmlElement(name="Person_Chg_Of_Addr_Date")
    public String personChgOfAddrDate;
    @XmlElement(name="Person_Postal_Code")
    public String personPostalCode;
    @XmlElement(name="Person_Blk_Hse_Nbr")
    public String personBlkHseNbr;
    @XmlElement(name="Person_Str_Name")
    public String personStrName;
    @XmlElement(name="Person_Level_Nbr")
    public String personLevelNbr;
    @XmlElement(name="Person_Unit_Nbr")
    public String personUnitNbr;
    @XmlElement(name="Person_Record_Last_Modified_Dttm")
    public String personRecordLastModifiedDttm;
    @XmlElement(name="Person_Bld_Name")
    public String personBldName;
    @XmlElement(name="Person_Previous_Id_Nbr")
    public String personPreviousIdNbr;
    @XmlElement(name="Person_Chg_Of_Addr_Trans_Nbr")
    public String personChgOfAddrTransNbr;
    @XmlElement(name="Person_Chg_Of_Id_Nbr_Date")
    public String personChgOfIdNbrDate;
    @XmlElement(name="Person_Id_Type")
    public String personIdType;
    @XmlElement(name="Person_Nationality")
    public String personNationality;;
}