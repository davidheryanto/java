package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dheryanto on 21/3/2016.
 */
@XmlRootElement(name = "CompanyPosition")
@XmlType(propOrder = {"header", "companyPositionRecords"})
public class CompanyPosition {
    @XmlElementWrapper(name="CompanyPositionTable")
    @XmlElement(name="companyPositionRecord")
    public List<CompanyPositionRecord> companyPositionRecords = new ArrayList<>();

    @XmlElement(name="Header")
    public Header header = new Header();

    public void add(CompanyPositionRecord companyPositionRecord) {
        companyPositionRecords.add(companyPositionRecord);
    }
}
