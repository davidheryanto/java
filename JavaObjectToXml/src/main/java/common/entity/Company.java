package common.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dheryanto on 18/3/2016.
 */
@XmlRootElement(name = "Company")
@XmlType(propOrder = {"header", "companyRecords"})
public class Company {
    @XmlElementWrapper(name="CompanyTable")
    @XmlElement(name="CompanyRecord")
    public List<CompanyRecord> companyRecords = new ArrayList<>();

    @XmlElement(name="Header")
    public Header header = new Header();

    public void add(CompanyRecord companyRecord) {
        companyRecords.add(companyRecord);
    }
}
